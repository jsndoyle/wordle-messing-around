
/**
 * Written by Jay Doyle (aka Aria aka jsndoyle@vt.edu)
 * Coding project based on https://www.powerlanguage.co.uk/wordle/
 * Base code from dms with Eiko (https://github.com/sparkle-preference)
 * 
 * Basically the idea was we wanted to figure out what the best strategy for wordle was, after it was linked in a discord.
 * I'm not sure this is it, but this was fun to write and I enjoy playing with C
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include "words.h"

#define WORD_LENGTH 5
#define WORD_COUNT 5757
#define WORD_CHOICE_COUNT 12792

static char FILE_WORDS[WORD_COUNT + 1][WORD_LENGTH + 1];

static char *firstguess = "tares"; //Calculated with ./words with no arguments

bool verbose = false;

static short counter_out[26];

/**
 * Counts how prevalent each lower case letter is in 'word'
 * 
 * 'word' is expected to be character array of size WORD_LENGTH, any characters past that size are ignored, 
 * if it terminates before that size that is also ignored (likely causing seg-fault)
 * 
 * counter_out is first zeroed out before being counted.
 * 
 * Relies on word containing only lower case letters, the inclusion of any other character will cause memory issues. 
 */
static void Counter(char *word)
{
    for (int i = 0; i < 26; i++)
    {
        counter_out[i] = 0;
    }
    for (int i = 0; i < WORD_LENGTH; i++)
    {
        counter_out[word[i] - 'a']++;
    }
}

/**
 * Helper function for Color Vectors (here represented as ternary numbers)
 * 
 * Assumes trit represented by 'slot' in 'val' is 0, sets it to value indicated by 'color' (should be between 0 and 2 inclusive)
 * 
 * Takes 'val' as input and returns new 'val'
 */
static int addColor(int val, int slot, int color)
{
    int x = 1;
    for (int i = 0; i < slot; i++)
    {
        x *= 3;
    }
    return val + color * x;
}

/**
 * Basic function of Wordle game.
 * 
 * Takes 'guess' word as input and checks vs 'secret_word' word outputing Color Vector as int
 * 
 * Both words expected to be of size WORD_LENGTH and consisting only of lower case characters (null terminated irrelevant)
 * 
 * Output Color Vector int is five trits
 * 
 * Sets values in counter_out
 */
static int checkGuess(char *guess, char *secret_word)
{
    int ret = 0;
    Counter(secret_word);
    for (int i = 0; i < WORD_LENGTH; i++)
    {
        if (guess[i] == secret_word[i])
        {
            ret = addColor(ret, i, 2); //Set green
            counter_out[guess[i] - 'a']--;
        }
    }
    for (int i = 0; i < WORD_LENGTH; i++)
    {
        if (guess[i] == secret_word[i])
            ;
        else if (counter_out[guess[i] - 'a'] > 0)
        {
            ret = addColor(ret, i, 1); //Set yellow
            counter_out[guess[i] - 'a']--;
        }
        else
        {
            ret = addColor(ret, i, 0); //Set grey
            counter_out[guess[i] - 'a']--;
        }
    }
    return ret;
}

/**
 * See checkGuess for parameter formatting.
 * Helper function, check if Color Vector 'colors' is the expected color vector for 'possible_word' with guess 'guess'
 * 
 * Same pre-conditions and assumptions as checkGuess
 */
static bool checkValidGuess(char *guess, int colors, char *possible_word)
{
    int truecolors = checkGuess(guess, possible_word);
    return colors == truecolors;
}

/**
 * filters 'list', removing any words that do not fit 'guess' with Color Vector 'colors'
 * if 'shouldFree', frees 'list', always allocates new char** 'list' that is returned.
 * 
 * If verbose, prints the number of removed entries and number of remaining entries as well.
 */
static char **filter(char **list, char *guess, int colors, bool shouldFree)
{
    int size = 1;
    char **ret = malloc(sizeof(char *) * size);
    int slot = 0;
    int iter = 0;
    int gone = 0;
    while (list[iter] != NULL)
    {
        if (checkValidGuess(guess, colors, list[iter]))
        {
            ret[slot] = list[iter];
            slot++;
            if (slot == size)
            {
                size *= 2;
                ret = realloc(ret, sizeof(char *) * size);
            }
        }
        else
        {
            gone++;
        }
        iter++;
    }

    if (verbose)
    {
        printf("Removed %d, now %d remain\n", gone, slot);
    }
    ret[slot] = NULL;
    if (shouldFree)
        free(list);
    return ret;
}

static int countRemoved(char **list, char *guess, int colors)
{
    int iter = 0;
    int gone = 0;
    while (list[iter] != NULL)
    {
        if (!checkValidGuess(guess, colors, list[iter]))
        {
            gone++;
        }
        iter++;
    }

    return gone;
}

static char **allocateFromFile(char *filename)
{
    FILE *file = fopen(filename, "r");
    int size = 1;
    int slot = 0;
    char **ret = malloc(sizeof(char *) * size);
    char *dummy = malloc(sizeof(char) * 5);
    dummy = "\n";
    while (dummy != NULL)
    {
        fgets(ret[slot], WORD_LENGTH + 1, file);
        fgets(dummy, 2, file);
        slot++;
        if (slot == size)
        {
            size *= 2;
            ret = realloc(ret, sizeof(char *) * size);
        }
    }
    ret[slot] = NULL;
    free(dummy);
    fclose(file);
    return ret;
}

/**
 * Sums the trits in Color Vector 'colors'
 */
static int sumVal(int colors)
{
    int ret = 0;
    for (int i = 0; i < WORD_LENGTH; i++)
    {
        ret += colors % 3;
        colors /= 3;
    }
    return ret;
}

/**
 * Calculates the score and 'subscore' of word 'word' with set of results 'entries' using calloc'd bool block 'block'
 */
static int calculateScore(char *word, char **entries, int *subscore, bool *block)
{
    int score = 0;
    int x = 0;
    while (entries[x] != NULL)
    {
        int colors = checkGuess(word, entries[x]);
        block[colors] = true;
        *subscore += sumVal(colors);
        x++;
    }
    for (int i = 0; i < 243; i++)
    {
        score += block[i];
        block[i] = 0;
    }
    return score;
}

/**
 * Writes Color Vector 'colors' into newly allocated char array representing in proper trit string format
 */
//Returns pointer to newly allocated memory
static char *generateColors(int colors)
{
    char *ret = malloc(sizeof(char) * (WORD_LENGTH + 1));
    for (int i = 0; i < WORD_LENGTH; i++)
    {
        ret[i] = colors % 3 == 2 ? '2' : (colors % 3 == 1 ? '1' : '0');
        colors /= 3;
    }
    ret[5] = '\0';
    return ret;
}

/**
 * Calculates the score of word with entries using short block
 */
static int calculateMaxOpts(char *word, char **entries, int *subscore, short *block, bool verbose)
{
    int x = 0;
    while (entries[x] != NULL)
    {
        int colors = checkGuess(word, entries[x]);
        block[colors]++;
        x++;
    }
    int maxOptions = 0;
    for (int i = 0; i < 243; i++)
    {
        if (verbose)
        {
            char *colorsAsWord = generateColors(i);
            printf("%s, %s has %d options\t", word, colorsAsWord, block[i]);
            free(colorsAsWord);
        }
        if (block[i] > maxOptions)
        {
            maxOptions = block[i];
            *subscore = i;
        }
        block[i] = 0;
    }
    maxOptions = WORD_COUNT - maxOptions;
    return maxOptions;
}

/**
 * From set of possible answers 'entries' and set of possible guesses 'FILE_WORDS', determine the best possible guess
 * 
 * If the set of possible answers contains only one entry, return it, because it's obviously the answer
 * 
 * Otherwise, set aside a boolean 'block' of size 243 (all the possible color vectors)
 * 
 * For each possible guess 'FILE_WORDS[i]', 
 *      for each possible answer 'entries[x]', 
 *          calculate the Color Vector 'colors' of that pairing, and set the corresponding bit in 'block' to true
 *      total the 'true' bits in 'block', that is this guess's score
 * return the guess with the greatest score
 * 
 */
static char *determineBestGuess(char **entries)
{
    char *bestguess = FILE_WORDS[0];
    int bestscore = 0;
    int bestsubscore = 0;
    if (entries[1] == NULL)
    {
        return entries[0];
    }
    bool *block = calloc(sizeof(bool), 243);
    for (int i = 0; i < WORD_COUNT; i++)
    {
        int subscore = 0;
        int score = calculateScore(FILE_WORDS[i], entries, &subscore, block);
        if (score > bestscore || (score == bestscore && subscore > bestsubscore))
        {
            bestscore = score;
            bestsubscore = subscore;
            bestguess = FILE_WORDS[i];
        }
    }
    free(block);
    return bestguess;
}

static char *determineBestGuessExpanded(char **entries)
{
    char *bestguess = CHOICE_WORDS[0];
    int bestscore = 0;
    int bestsubscore = 0;
    if (entries[1] == NULL)
    {
        return entries[0];
    }
    bool *block = calloc(sizeof(bool), 243);
    for (int i = 0; i < WORD_CHOICE_COUNT; i++)
    {
        int subscore = 0;
        int score = calculateScore(CHOICE_WORDS[i], entries, &subscore, block);
        if (score > bestscore || (score == bestscore && subscore > bestsubscore))
        {
            bestscore = score;
            bestsubscore = subscore;
            bestguess = CHOICE_WORDS[i];
        }
    }
    free(block);
    return bestguess;
}

/**
 * Read the input file with the name 'filename' with line count WORD_COUNT and with words of length WORD_LENGTH to FILE_WORDS
 */
static void readInput(char *filename)
{
    FILE *file = fopen(filename, "r");
    char *dummy = malloc(sizeof(char) * 5);
    for (int i = 0; i < WORD_COUNT; i++)
    {
        fgets(FILE_WORDS[i], WORD_LENGTH + 1, file);
        fgets(dummy, 2, file);
    }
    free(dummy);
    fclose(file);
}

static void readInputToChoice(char *filename)
{
    FILE *file = fopen(filename, "r");
    char *dummy = malloc(sizeof(char) * 5);
    for (int i = 0; i < WORD_CHOICE_COUNT; i++)
    {
        fgets(CHOICE_WORDS[i], WORD_LENGTH + 1, file);
        printf("Got %s\n", CHOICE_WORDS[i]);
        fgets(dummy, 2, file);
    }
    free(dummy);
    fclose(file);
}

//Returns pointer to newly allocated memory
static char **generateGuesses()
{
    char **ret = malloc(sizeof(char *) * (WORD_COUNT + 1));
    int i = 0;
    for (i = 0; i < WORD_CHOICE_COUNT; i++)
    {
        ret[i] = CHOICE_WORDS[i];
    }
    ret[i] = NULL;
    return ret;
}
//Returns pointer to newly allocated memory
static char **generateAnswers()
{
    char **ret = malloc(sizeof(char *) * (WORD_COUNT + 1));
    int i = 0;
    for (i = 0; i < 2315; i++)
    {
        ret[i] = ANSWER_WORDS[i];
    }
    ret[i] = NULL;
    return ret;
}

/**
 * Copies the contents of 2 dimensional array FILE_WORDS to newly allocated double pointer that is returned
 */
//Returns pointer to newly allocated memory
static char **generateEntries()
{
    char **ret = malloc(sizeof(char *) * (WORD_COUNT + 1));
    int i = 0;
    for (i = 0; i < WORD_COUNT; i++)
    {
        ret[i] = FILE_WORDS[i];
    }
    ret[i] = NULL;
    return ret;
}

/**
 * Tests the algorithm against word 'word'
 * If verbose is true, prints lots of wordy output
 * Otherwise, the only output will be a print if the word is not in the dictionary, or returning the number of guesses that it took
 */
static int testOnWord(char *word)
{
    char **entries = generateEntries();
    if (verbose)
    {
        printf("Starting, word is %s\n", word);
        fflush(STDIN_FILENO);
    }
    else
    {
        printf("%s\t", word);
        fflush(STDIN_FILENO);
    }
    int count = 10;
    bool found = false;

    while (!found && count > 0)
    {
        count--;
        char *guess = count == 9 ? firstguess : determineBestGuessExpanded(entries);
        if (guess == NULL)
        {
            printf("Word does not exist in dictionary\n");
            fflush(STDIN_FILENO);
            return 0;
        }
        int colors = checkGuess(guess, word);

        if (verbose)
        {
            char *colorsAsWord = generateColors(colors);
            printf("Guessed %s, Got %s, count %d\n", guess, colorsAsWord, sumVal(colors));
            free(colorsAsWord);
        }
        if (sumVal(colors) == 2 * WORD_LENGTH)
        {
            if (verbose)
            {
                printf("Guess is correct, complete\n");
            }
            found = true;
        }
        else
        {
            entries = filter(entries, guess, colors, true);
        }

        if (verbose)
        {
            fflush(STDIN_FILENO);
        }
    }
    free(entries);
    return 10 - count;
}

static int testOnWordAnswer(char *word)
{
    char **entries = generateAnswers();
    if (verbose)
    {
        printf("Starting, word is %s\n", word);
        fflush(STDIN_FILENO);
    }
    else
    {
        printf("%s\t", word);
        fflush(STDIN_FILENO);
    }
    int count = 10;
    bool found = false;

    while (!found && count > 0)
    {
        count--;
        char *guess = count == 9 ? firstguess : determineBestGuessExpanded(entries);
        printf("\t%s", guess);
        if (guess == NULL)
        {
            printf("Word does not exist in dictionary\n");
            fflush(STDIN_FILENO);
            return 0;
        }
        int colors = checkGuess(guess, word);

        if (verbose)
        {
            char *colorsAsWord = generateColors(colors);
            printf("Guessed %s, Got %s, count %d\n", guess, colorsAsWord, sumVal(colors));
            free(colorsAsWord);
        }
        if (sumVal(colors) == 2 * WORD_LENGTH)
        {
            if (verbose)
            {
                printf("Guess is correct, complete\n");
            }
            found = true;
        }
        else
        {
            entries = filter(entries, guess, colors, true);
        }

        if (verbose)
        {
            fflush(STDIN_FILENO);
        }
    }
    int ret = count;
    while (count > 1)
    {
        printf("\t");
        count--;
    }
    free(entries);
    return 10 - ret;
}

struct guess
{
    int val;
    int subval;
    char *word;
};
int cmpguess(const void *a, const void *b)
{
    int diff = ((struct guess *)b)->val - ((struct guess *)a)->val;
    if (diff == 0)
    {
        return ((struct guess *)b)->subval - ((struct guess *)a)->subval;
    }
    else
    {
        return diff;
    }
}

/**
 * Same as testOnWord, but now using words from STDIN instead of from algorithm, WIP. still some kinks to work out, STDIN is annoying to work with.
 */
static int testOnWordManual(char *word)
{
restart:
    printf("Starting\n");
    char **entries = generateEntries();
    fflush(STDIN_FILENO);
    bool found = false;
    int nextScore = -1;
    while (!found)
    {
        char *input = malloc(sizeof(char) * 6);
        bool skip = false;
        int count = 0;
        for (int i = 0; i < 5 && !skip; i++) //This feels redundant but other setups have mysterious bugs, I suspect that I am failing to understand C
        {
            input[i] = getchar();
            if (input[i] == '\n')
            {
                input[i] = '\0';
                skip = true;
                continue;
            }
        }
        if (!skip)
        {
            input[5] = '\0';
            while (getchar() != '\n')
                ;
        }
        bool isTrick = true;
        for (int i = 0; i < 5; i++)
        {
            if (input[i] < 'a' || input[i] > 'z')
                skip = true;
            if (input[i] != '0' && input[i] != '1' && input[i] != '2')
                isTrick = false;
        }
        if (skip)
        {
            count = atoi(input + 1);
            if (input[0] == '?')
            {
                printf("Printing list of available words\n");
                if (count == 0)
                    count = WORD_COUNT;
                for (int i = 0; entries[i] != NULL && i < count; i++)
                {
                    printf("%s\n", entries[i]);
                }
                printf("Possible answers remaining listed above\n");
            }
            else if (input[0] == '=')
            {
                bool *block = calloc(sizeof(bool), 243);
                struct guess *guesses = malloc(sizeof(struct guess) * WORD_COUNT);
                for (int i = 0; i < WORD_COUNT; i++)
                {
                    guesses[i].val = calculateScore(FILE_WORDS[i], entries, &(guesses[i].subval), block);
                    guesses[i].word = FILE_WORDS[i];
                }
                free(block);
                qsort(guesses, WORD_COUNT, sizeof(struct guess), cmpguess);
                printf("Printing list of guesses with scores and subscores\n");
                for (int i = 0; (count == 0 || i < count) && i < WORD_COUNT; i++)
                {
                    printf(" %s:%03d & %05d\t", guesses[i].word, guesses[i].val, guesses[i].subval);
                }
                printf("\nPossible guesses listed above\n");
                free(guesses);
            }
            else if (input[0] == '-')
            {
                int numRemaining = 0;
                while (entries[numRemaining] != NULL)
                    numRemaining++;
                short *block = calloc(sizeof(short), 243);
                struct guess *guesses = malloc(sizeof(struct guess) * WORD_COUNT);
                for (int i = 0; i < WORD_COUNT; i++)
                {
                    guesses[i].val = calculateMaxOpts(FILE_WORDS[i], entries, &(guesses[i].subval), block, false);
                    guesses[i].word = FILE_WORDS[i];
                }
                free(block);
                qsort(guesses, WORD_COUNT, sizeof(struct guess), cmpguess);
                //calculateMaxOpts(guesses[0].word, entries, &(guesses[0].subval), block, true);
                printf("Printing list of worst guesses with scores and subscores\n");
                for (int i = 0; (count == 0 || i < count) && i < WORD_COUNT; i++)
                {
                    char *colorsAsWord = generateColors(guesses[i].subval);
                    printf(" %s:%03d & %s\t", guesses[i].word, guesses[i].val, colorsAsWord);
                    free(colorsAsWord);
                }
                printf("\nPossible worst guesses listed above\n");
                free(guesses);
            }
            else if (isTrick)
            {
                nextScore = 0;
                int power = 1;
                for (int i = 0; i < 5; i++)
                {
                    nextScore += (input[i] - '0') * power;
                    power *= 3;
                }
                printf("Score override received, using %s (%d) as score for next entry\n", input, nextScore);
            }
            else if (input[0] == '#' && input[1] == '#' && input[2] == '#' && input[3] == '#' && input[4] == '#')
            {
                printf("Restarting\n");
                free(input);
                free(entries);
                goto restart;
            }
            else
            {
                printf("Invalid entry '%s' :(\n", input);
            }
        }
        else
        {
            int colors = checkGuess(input, word);
            if (nextScore != -1)
            {
                colors = nextScore;
                nextScore = -1;
            }
            char *colorsAsWord = generateColors(colors);
            printf("Guessed %s, Got %s, count %d\n", input, colorsAsWord, sumVal(colors));
            free(colorsAsWord);
            if (sumVal(colors) == 2 * WORD_LENGTH)
            {
                printf("Guess is correct, complete\n");
                found = true;
            }
            else
            {
                entries = filter(entries, input, colors, true);
            }
        }
        fflush(STDIN_FILENO);
        free(input);
    }
    free(entries);
    return 0;
}

static int testOnWordSpecial(char *word)
{
restart:
    printf("Starting\n");
    char **entries = generateEntries();
    fflush(STDIN_FILENO);
    bool found = false;
    int nextScore = -1;
    while (!found)
    {
        char *input = malloc(sizeof(char) * 6);
        bool skip = false;
        int count = 0;
        for (int i = 0; i < 5 && !skip; i++) //This feels redundant but other setups have mysterious bugs, I suspect that I am failing to understand C
        {
            input[i] = getchar();
            if (input[i] == '\n')
            {
                input[i] = '\0';
                skip = true;
                continue;
            }
        }
        if (!skip)
        {
            input[5] = '\0';
            while (getchar() != '\n')
                ;
        }
        bool isTrick = true;
        for (int i = 0; i < 5; i++)
        {
            if (input[i] < 'a' || input[i] > 'z')
                skip = true;
            if (input[i] != '0' && input[i] != '1' && input[i] != '2')
                isTrick = false;
        }
        if (skip)
        {
            count = atoi(input + 1);
            if (input[0] == '?')
            {
                printf("Printing list of available words\n");
                if (count == 0)
                    count = WORD_COUNT;
                for (int i = 0; entries[i] != NULL && i < count; i++)
                {
                    printf("%s\n", entries[i]);
                }
                printf("Possible answers remaining listed above\n");
            }
            else if (input[0] == '=')
            {
                bool *block = calloc(sizeof(bool), 243);
                struct guess *guesses = malloc(sizeof(struct guess) * WORD_COUNT);
                for (int i = 0; i < WORD_COUNT; i++)
                {
                    guesses[i].val = calculateScore(FILE_WORDS[i], entries, &(guesses[i].subval), block);
                    guesses[i].word = FILE_WORDS[i];
                }
                free(block);
                qsort(guesses, WORD_COUNT, sizeof(struct guess), cmpguess);
                printf("Printing list of guesses with scores and subscores\n");
                for (int i = 0; (count == 0 || i < count) && i < WORD_COUNT; i++)
                {
                    printf(" %s:%03d & %05d\t", guesses[i].word, guesses[i].val, guesses[i].subval);
                }
                printf("\nPossible guesses listed above\n");
                free(guesses);
            }
            else if (input[0] == '-')
            {
                int numRemaining = 0;
                while (entries[numRemaining] != NULL)
                    numRemaining++;
                short *block = calloc(sizeof(short), 243);
                struct guess *guesses = malloc(sizeof(struct guess) * WORD_COUNT);
                for (int i = 0; i < WORD_COUNT; i++)
                {
                    guesses[i].val = calculateMaxOpts(FILE_WORDS[i], entries, &(guesses[i].subval), block, false);
                    guesses[i].word = FILE_WORDS[i];
                }
                free(block);
                qsort(guesses, WORD_COUNT, sizeof(struct guess), cmpguess);
                //calculateMaxOpts(guesses[0].word, entries, &(guesses[0].subval), block, true);
                printf("Printing list of worst guesses with scores and subscores\n");
                for (int i = 0; (count == 0 || i < count) && i < WORD_COUNT; i++)
                {
                    char *colorsAsWord = generateColors(guesses[i].subval);
                    printf(" %s:%03d & %s\t", guesses[i].word, guesses[i].val, colorsAsWord);
                    free(colorsAsWord);
                }
                printf("\nPossible worst guesses listed above\n");
                free(guesses);
            }
            else if (isTrick)
            {
                nextScore = 0;
                int power = 1;
                for (int i = 0; i < 5; i++)
                {
                    nextScore += (input[i] - '0') * power;
                    power *= 3;
                }
                printf("Score override received, using %s (%d) as score for next entry\n", input, nextScore);
            }
            else if (input[0] == '#' && input[1] == '#' && input[2] == '#' && input[3] == '#' && input[4] == '#')
            {
                printf("Restarting\n");
                free(input);
                free(entries);
                goto restart;
            }
            else
            {
                printf("Invalid entry '%s' :(\n", input);
            }
        }
        else
        {
            int colors = checkGuess(input, word);
            if (nextScore != -1)
            {
                colors = nextScore;
                nextScore = -1;
            }
            char *colorsAsWord = generateColors(colors);
            printf("Guessed %s, Got %s, count %d\n", input, colorsAsWord, sumVal(colors));
            free(colorsAsWord);
            if (sumVal(colors) == 2 * WORD_LENGTH)
            {
                printf("Guess is correct, complete\n");
                found = true;
            }
            else
            {
                entries = filter(entries, input, colors, true);
            }
        }
        fflush(STDIN_FILENO);
        free(input);
    }
    free(entries);
    return 0;
}

static char **getBestGuess(char **entries, char *word, int colors, char **bestGuess, int *count)
{
    char **newentries = filter(entries, word, colors, false);
    int x = 0;
    while (newentries[x] != NULL)
        x++;
    *count = x;
    *bestGuess = determineBestGuessExpanded(newentries);
    return newentries;
}

static void permutePossibilities(char **entries, char *word, int depth)
{
    //printf("Recursing\n");
    fflush(STDIN_FILENO);
    for (int i = 0; i < 242; i++)
    {
        char *colorsAsWord = generateColors(i);
        int count = 0;
        char *bestGuess = "00000";
        char **newentries = getBestGuess(entries, word, i, &bestGuess, &count);
        if (count == 0)
        {
            //printf("%s\t%s\tnothing left\n", word, colorsAsWord);
            free(newentries);
            fflush(STDIN_FILENO);
            free(colorsAsWord);
        }
        else
        {
            printf("%*s%s\t%s\t,\t%03d\t%s\n", depth, "", word, colorsAsWord, count, bestGuess);
            fflush(STDIN_FILENO);
            free(colorsAsWord);
            if (count != 1)
            {
                permutePossibilities(newentries, bestGuess, depth + 1);
            }
            free(newentries);
        }
    }
}

/**
 * Prints usage of this code in terminal
 */
static void
usage(char *av0)
{
    fprintf(stderr, "Usage: %s [-w word] [-a] [-n number]\n"
                    "  -w word      real word to use\n"
                    "  -q guess     guess to use\n"
                    "  -f           iteratively map possibilities\n"
                    "  -a           to iteratively go through all words\n"
                    "  -n number    to iteratively go through the first number words\n"
                    "  -i           stdin entry mode. Input is expected to be five character words, if no word is given will use random word\n"
                    "  -h           display this help\n",
            av0);
    exit(EXIT_FAILURE);
}

int main(int ac, char *av[])
{
    int opt;
    char *word = NULL;
    char *guess = NULL;
    int number = 0;
    bool manualmode = false;
    bool fullauto = false;
    while ((opt = getopt(ac, av, "ahifn:w:q:")) != -1)
    {
        switch (opt)
        {
        case 'a':
            number = WORD_COUNT;
            break;
        case 'i':
            manualmode = true;
            break;
        case 'f':
            fullauto = true;
            break;
        case 'n':
            number = atoi(optarg);
            break;
        case 'w':
            word = optarg;
            break;
        case 'q':
            guess = optarg;
            break;
        case 'h':
        default: /* '?' */
            usage(av[0]);
        }
    }
    readInput("sgb-words.txt");

    fflush(STDIN_FILENO);

    if (guess != NULL && word != NULL)
    {
        int colors = checkGuess(guess, word);
        char *colorsAsWord = generateColors(colors);
        printf("Guessed %s, Got %s, Word was %s", guess, colorsAsWord, word);
        free(colorsAsWord);
    }
    else if (manualmode)
    {
        verbose = true;
        if (word == NULL)
        {
            srand(time(NULL));
            int r = rand();
            word = FILE_WORDS[r % WORD_COUNT];
        }
        testOnWordManual(word);
    }
    else if (word != NULL)
    {
        verbose = true;
        testOnWord(word);
    }
    else if (number != 0)
    {
        for (int i = 0; i < number && i < 2315; i++)
        {
            int count = testOnWordAnswer(ANSWER_WORDS[i]);
            printf("%d\n", count);
            fflush(STDIN_FILENO);
        }
    }
    else if (fullauto)
    {
        char **entries = generateEntries();
        permutePossibilities(entries, "tares", 0);
        free(entries);
    }
    else
    {
        printf("Starting 1!\n");
        fflush(STDIN_FILENO);
        char **entries = generateEntries();
        char *result = determineBestGuessExpanded(entries);
        printf("Found %s\n", result);
        free(entries);
        printf("Starting 2\n");
        fflush(STDIN_FILENO);
        entries = generateEntries();
        char *bestword = FILE_WORDS[0];
        int bestscore = 0;
        for (int i = 0; i < WORD_COUNT; i++)
        {
            int score = countRemoved(entries, FILE_WORDS[i], 0);
            if (score > bestscore)
            {
                bestscore = score;
                bestword = FILE_WORDS[i];
                printf("New best: %s with %d\n", FILE_WORDS[i], score);
                fflush(STDIN_FILENO);
            }
        }
        free(entries);
    }
    fflush(STDIN_FILENO);
    exit(EXIT_SUCCESS);
}
